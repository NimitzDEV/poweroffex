﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAbout
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.formskin1 = New 定时关机Ex.FormSkin()
        Me.FlatLabel9 = New 定时关机Ex.FlatLabel()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.FlatLabel8 = New 定时关机Ex.FlatLabel()
        Me.FlatButton1 = New 定时关机Ex.FlatButton()
        Me.FlatLabel7 = New 定时关机Ex.FlatLabel()
        Me.FlatLabel6 = New 定时关机Ex.FlatLabel()
        Me.FlatLabel5 = New 定时关机Ex.FlatLabel()
        Me.FlatLabel4 = New 定时关机Ex.FlatLabel()
        Me.FlatLabel3 = New 定时关机Ex.FlatLabel()
        Me.FlatLabel2 = New 定时关机Ex.FlatLabel()
        Me.FlatLabel1 = New 定时关机Ex.FlatLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.LinkLabel5 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel4 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel8 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel9 = New System.Windows.Forms.LinkLabel()
        Me.FlatLabel10 = New 定时关机Ex.FlatLabel()
        Me.LinkLabel3 = New System.Windows.Forms.LinkLabel()
        Me.formskin1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'formskin1
        '
        Me.formskin1.BackColor = System.Drawing.Color.White
        Me.formskin1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.formskin1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(53, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(60, Byte), Integer))
        Me.formskin1.Controls.Add(Me.LinkLabel3)
        Me.formskin1.Controls.Add(Me.FlatLabel10)
        Me.formskin1.Controls.Add(Me.FlatLabel9)
        Me.formskin1.Controls.Add(Me.LinkLabel1)
        Me.formskin1.Controls.Add(Me.FlatLabel8)
        Me.formskin1.Controls.Add(Me.FlatButton1)
        Me.formskin1.Controls.Add(Me.FlatLabel7)
        Me.formskin1.Controls.Add(Me.FlatLabel6)
        Me.formskin1.Controls.Add(Me.FlatLabel5)
        Me.formskin1.Controls.Add(Me.FlatLabel4)
        Me.formskin1.Controls.Add(Me.FlatLabel3)
        Me.formskin1.Controls.Add(Me.FlatLabel2)
        Me.formskin1.Controls.Add(Me.FlatLabel1)
        Me.formskin1.Controls.Add(Me.PictureBox1)
        Me.formskin1.Controls.Add(Me.LinkLabel5)
        Me.formskin1.Controls.Add(Me.LinkLabel4)
        Me.formskin1.Controls.Add(Me.LinkLabel2)
        Me.formskin1.Controls.Add(Me.LinkLabel8)
        Me.formskin1.Controls.Add(Me.LinkLabel9)
        Me.formskin1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.formskin1.FlatColor = System.Drawing.Color.Orange
        Me.formskin1.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.formskin1.HeaderColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.formskin1.HeaderMaximize = False
        Me.formskin1.Location = New System.Drawing.Point(0, 0)
        Me.formskin1.Name = "formskin1"
        Me.formskin1.Size = New System.Drawing.Size(280, 312)
        Me.formskin1.TabIndex = 28
        Me.formskin1.Text = "关于"
        '
        'FlatLabel9
        '
        Me.FlatLabel9.AutoSize = True
        Me.FlatLabel9.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel9.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel9.ForeColor = System.Drawing.Color.White
        Me.FlatLabel9.Location = New System.Drawing.Point(65, 541)
        Me.FlatLabel9.Name = "FlatLabel9"
        Me.FlatLabel9.Size = New System.Drawing.Size(144, 13)
        Me.FlatLabel9.TabIndex = 37
        Me.FlatLabel9.Text = "nimitzdevteam@gmail.com"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.LinkColor = System.Drawing.Color.DarkOrange
        Me.LinkLabel1.Location = New System.Drawing.Point(65, 279)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(137, 15)
        Me.LinkLabel1.TabIndex = 36
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "通过支付宝二维码捐助"
        '
        'FlatLabel8
        '
        Me.FlatLabel8.AutoSize = True
        Me.FlatLabel8.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel8.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel8.ForeColor = System.Drawing.Color.White
        Me.FlatLabel8.Location = New System.Drawing.Point(36, 255)
        Me.FlatLabel8.Name = "FlatLabel8"
        Me.FlatLabel8.Size = New System.Drawing.Size(202, 13)
        Me.FlatLabel8.TabIndex = 35
        Me.FlatLabel8.Text = "如果你喜欢本应用，欢迎捐助我哦"
        '
        'FlatButton1
        '
        Me.FlatButton1.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton1.BaseColor = System.Drawing.Color.OrangeRed
        Me.FlatButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton1.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatButton1.Location = New System.Drawing.Point(211, 9)
        Me.FlatButton1.Name = "FlatButton1"
        Me.FlatButton1.Rounded = False
        Me.FlatButton1.Size = New System.Drawing.Size(51, 32)
        Me.FlatButton1.TabIndex = 34
        Me.FlatButton1.Text = "OK"
        Me.FlatButton1.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'FlatLabel7
        '
        Me.FlatLabel7.AutoSize = True
        Me.FlatLabel7.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel7.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel7.ForeColor = System.Drawing.Color.White
        Me.FlatLabel7.Location = New System.Drawing.Point(15, 231)
        Me.FlatLabel7.Name = "FlatLabel7"
        Me.FlatLabel7.Size = New System.Drawing.Size(249, 13)
        Me.FlatLabel7.TabIndex = 33
        Me.FlatLabel7.Text = "——————————————————————"
        '
        'FlatLabel6
        '
        Me.FlatLabel6.AutoSize = True
        Me.FlatLabel6.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel6.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatLabel6.ForeColor = System.Drawing.Color.White
        Me.FlatLabel6.Location = New System.Drawing.Point(110, 176)
        Me.FlatLabel6.Name = "FlatLabel6"
        Me.FlatLabel6.Size = New System.Drawing.Size(52, 21)
        Me.FlatLabel6.TabIndex = 32
        Me.FlatLabel6.Text = "ImTail"
        '
        'FlatLabel5
        '
        Me.FlatLabel5.AutoSize = True
        Me.FlatLabel5.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel5.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatLabel5.ForeColor = System.Drawing.Color.White
        Me.FlatLabel5.Location = New System.Drawing.Point(19, 176)
        Me.FlatLabel5.Name = "FlatLabel5"
        Me.FlatLabel5.Size = New System.Drawing.Size(85, 21)
        Me.FlatLabel5.TabIndex = 31
        Me.FlatLabel5.Text = "want_旺旺"
        '
        'FlatLabel4
        '
        Me.FlatLabel4.AutoSize = True
        Me.FlatLabel4.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel4.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatLabel4.ForeColor = System.Drawing.Color.White
        Me.FlatLabel4.Location = New System.Drawing.Point(197, 147)
        Me.FlatLabel4.Name = "FlatLabel4"
        Me.FlatLabel4.Size = New System.Drawing.Size(59, 21)
        Me.FlatLabel4.TabIndex = 30
        Me.FlatLabel4.Text = "Credits"
        '
        'FlatLabel3
        '
        Me.FlatLabel3.AutoSize = True
        Me.FlatLabel3.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel3.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel3.ForeColor = System.Drawing.Color.White
        Me.FlatLabel3.Location = New System.Drawing.Point(15, 134)
        Me.FlatLabel3.Name = "FlatLabel3"
        Me.FlatLabel3.Size = New System.Drawing.Size(249, 13)
        Me.FlatLabel3.TabIndex = 29
        Me.FlatLabel3.Text = "——————————————————————"
        '
        'FlatLabel2
        '
        Me.FlatLabel2.AutoSize = True
        Me.FlatLabel2.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel2.Font = New System.Drawing.Font("Segoe UI", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatLabel2.ForeColor = System.Drawing.Color.White
        Me.FlatLabel2.Location = New System.Drawing.Point(15, 89)
        Me.FlatLabel2.Name = "FlatLabel2"
        Me.FlatLabel2.Size = New System.Drawing.Size(171, 45)
        Me.FlatLabel2.TabIndex = 28
        Me.FlatLabel2.Text = "NimitzDEV"
        '
        'FlatLabel1
        '
        Me.FlatLabel1.AutoSize = True
        Me.FlatLabel1.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatLabel1.ForeColor = System.Drawing.Color.White
        Me.FlatLabel1.Location = New System.Drawing.Point(19, 68)
        Me.FlatLabel1.Name = "FlatLabel1"
        Me.FlatLabel1.Size = New System.Drawing.Size(92, 21)
        Me.FlatLabel1.TabIndex = 27
        Me.FlatLabel1.Text = "Powered by"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.定时关机Ex.My.Resources.Resources.alipayqrcode
        Me.PictureBox1.Location = New System.Drawing.Point(23, 314)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(234, 224)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 26
        Me.PictureBox1.TabStop = False
        '
        'LinkLabel5
        '
        Me.LinkLabel5.AutoSize = True
        Me.LinkLabel5.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel5.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel5.LinkColor = System.Drawing.Color.DarkOrange
        Me.LinkLabel5.Location = New System.Drawing.Point(103, 206)
        Me.LinkLabel5.Name = "LinkLabel5"
        Me.LinkLabel5.Size = New System.Drawing.Size(59, 15)
        Me.LinkLabel5.TabIndex = 25
        Me.LinkLabel5.TabStop = True
        Me.LinkLabel5.Text = "新浪微博"
        '
        'LinkLabel4
        '
        Me.LinkLabel4.AutoSize = True
        Me.LinkLabel4.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel4.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel4.LinkColor = System.Drawing.Color.DarkOrange
        Me.LinkLabel4.Location = New System.Drawing.Point(20, 206)
        Me.LinkLabel4.Name = "LinkLabel4"
        Me.LinkLabel4.Size = New System.Drawing.Size(59, 15)
        Me.LinkLabel4.TabIndex = 22
        Me.LinkLabel4.TabStop = True
        Me.LinkLabel4.Text = "百度空间"
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel2.Font = New System.Drawing.Font("Segoe UI", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel2.LinkColor = System.Drawing.Color.DarkOrange
        Me.LinkLabel2.Location = New System.Drawing.Point(187, 70)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(65, 19)
        Me.LinkLabel2.TabIndex = 9
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "新浪微博"
        '
        'LinkLabel8
        '
        Me.LinkLabel8.AutoSize = True
        Me.LinkLabel8.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel8.Font = New System.Drawing.Font("Segoe UI", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel8.LinkColor = System.Drawing.Color.DarkOrange
        Me.LinkLabel8.Location = New System.Drawing.Point(187, 89)
        Me.LinkLabel8.Name = "LinkLabel8"
        Me.LinkLabel8.Size = New System.Drawing.Size(37, 19)
        Me.LinkLabel8.TabIndex = 13
        Me.LinkLabel8.TabStop = True
        Me.LinkLabel8.Text = "知乎"
        '
        'LinkLabel9
        '
        Me.LinkLabel9.AutoSize = True
        Me.LinkLabel9.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel9.Font = New System.Drawing.Font("Segoe UI", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel9.LinkColor = System.Drawing.Color.DarkOrange
        Me.LinkLabel9.Location = New System.Drawing.Point(187, 110)
        Me.LinkLabel9.Name = "LinkLabel9"
        Me.LinkLabel9.Size = New System.Drawing.Size(63, 19)
        Me.LinkLabel9.TabIndex = 14
        Me.LinkLabel9.TabStop = True
        Me.LinkLabel9.Text = "Google+"
        '
        'FlatLabel10
        '
        Me.FlatLabel10.AutoSize = True
        Me.FlatLabel10.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel10.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FlatLabel10.ForeColor = System.Drawing.Color.White
        Me.FlatLabel10.Location = New System.Drawing.Point(168, 176)
        Me.FlatLabel10.Name = "FlatLabel10"
        Me.FlatLabel10.Size = New System.Drawing.Size(94, 21)
        Me.FlatLabel10.TabIndex = 38
        Me.FlatLabel10.Text = "VirginiaILBX"
        '
        'LinkLabel3
        '
        Me.LinkLabel3.AutoSize = True
        Me.LinkLabel3.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel3.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel3.LinkColor = System.Drawing.Color.DarkOrange
        Me.LinkLabel3.Location = New System.Drawing.Point(179, 206)
        Me.LinkLabel3.Name = "LinkLabel3"
        Me.LinkLabel3.Size = New System.Drawing.Size(59, 15)
        Me.LinkLabel3.TabIndex = 39
        Me.LinkLabel3.TabStop = True
        Me.LinkLabel3.Text = "新浪微博"
        '
        'frmAbout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(280, 312)
        Me.ControlBox = False
        Me.Controls.Add(Me.formskin1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmAbout"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "关于"
        Me.TopMost = True
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.formskin1.ResumeLayout(False)
        Me.formskin1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LinkLabel9 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel8 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel4 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel5 As System.Windows.Forms.LinkLabel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents formskin1 As 定时关机Ex.FormSkin
    Friend WithEvents FlatButton1 As 定时关机Ex.FlatButton
    Friend WithEvents FlatLabel7 As 定时关机Ex.FlatLabel
    Friend WithEvents FlatLabel6 As 定时关机Ex.FlatLabel
    Friend WithEvents FlatLabel5 As 定时关机Ex.FlatLabel
    Friend WithEvents FlatLabel4 As 定时关机Ex.FlatLabel
    Friend WithEvents FlatLabel3 As 定时关机Ex.FlatLabel
    Friend WithEvents FlatLabel2 As 定时关机Ex.FlatLabel
    Friend WithEvents FlatLabel1 As 定时关机Ex.FlatLabel
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents FlatLabel8 As 定时关机Ex.FlatLabel
    Friend WithEvents FlatLabel9 As 定时关机Ex.FlatLabel
    Friend WithEvents LinkLabel3 As System.Windows.Forms.LinkLabel
    Friend WithEvents FlatLabel10 As 定时关机Ex.FlatLabel
End Class
